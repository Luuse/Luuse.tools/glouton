<?php

  ini_set('display_errors', 'Off'); # mode debug, off sur serveur !!!!

  include('./functions/global.php');
  include('./functions/ressources.php');
  include('./functions/typotheque.php');
  include('./functions/blog.php');
  include('./functions/git.php');
  include('./functions/screenshots.php');

  $getContent = new GetContent;

  $urls = [
    "http://www.luuse.io/Shaarli?do=rss" => 'Ressources',
    "http://typotheque.luuse.io/content/fonts/globalJson.json" => 'Typotheque',
    //  "http://localhost/www/2018/luuse.io/blog.luuse.io/grav-admin/" => 'Blog',
    "1917441" => 'Git', // general
    "2146438" => 'Git', // foundry
    "2126033" => 'Git', // tools
    "2052442" => 'Git', // luuse.io
    "2052241" => 'Git', // villa
    "http://localhost/www/2019/luuse.io/screenshots.luuse.io/json/screenshots.json" => 'Screenshots' // villa
  ];

  foreach ($urls as $url => $class) {
      $getContent->writeJson($url, $class);
  }

  $getContent->MergeGits();
  $getContent->getGlobalContent();
