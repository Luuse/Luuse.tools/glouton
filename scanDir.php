<?php

  include('inc/head.php');

  $settings = array(
      'baseUri' => $url,
      'userName' => $user,
      'password' => $pw
  );

  $client = new Sabre\DAV\Client($settings);

  $folder_content = $client->propFind('remote.php/webdav/taf/screenshots/', array(),1);

  $filesNames = array();
  $countAdd = 0;
  foreach ($folder_content as $path => $content) {
    echo $content;
    $fullUrl = 'http://claude.luuse.io' . $path;
    $fileName = substr($path, strrpos($path, '/') + 1);
    $fileNameDecoded = urldecode($fileName);
    $filesNames[] = urldecode($fileName);
    if (!file_exists($destinationDir . '/' . $fileNameDecoded)) {
	 $ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $fullUrl);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
	curl_setopt($ch, CURLOPT_USERPWD, $user . ":" . $pw);
	$result = curl_exec($ch);
	curl_close($ch);
      file_put_contents($destinationDir . '/' . $fileNameDecoded, $result);
       $imgData = resize_image($destinationDir . '/' . $fileNameDecoded, 400, 400);
      imagepng($imgData, $destinationDirThumb . "/" . $fileNameDecoded);
      unset($filesNames[$fileNameDecoded]);
      $countAdd += 1;
    }
  }
  // wget -nc -P assets/screenshots --user=zf --password=ezg' http://claude.luuse.io/remote.php/webdav/taf/screenshots/Capture%20d%e2%80%99%c3%a9cran%20de%202019-03-30%2019-13-42.png

  $countDel = 0;
  $files = scandir($destinationDir);
  foreach ($files as $file) {
    $ext = strtolower(pathinfo($file, PATHINFO_EXTENSION));
    if ($ext == 'png' || $ext == 'jpg' || $ext == 'jpeg') {
      // $fileNameOriginal = end(explode("__", $file));
      if (!in_array($file, $filesNames)) {
        unlink($destinationDir . '/' . $file);
        unlink($destinationDirThumb . '/' . $file);
        $countDel += 1;
      }
    }
  }

  echo '<p>' . $countAdd . ' files added and ' . $countDel . ' deleted.</p>';
