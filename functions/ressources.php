<?php

  class Ressources extends GetContent  {

    function getContent($url){
      $content = file_get_contents($url);
      $content = new SimpleXmlElement($content);
      $items = $content->channel->item;
      $ressources = array();
      foreach ($items as $i => $item) {
        $uniqId = uniqid();
        unset($item->description);
        $date = date('Ymdhm', strtotime($item->pubDate));
        $type = ["type" => 'ressources'];
        $item = (array) $item;
        array_push($item, $type);
        $ressources[$date . '-' . $uniqId] = $item;
      }
      $ressources = json_encode($ressources);
      return $ressources;
    }

  }
