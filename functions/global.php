<?php

class GetContent {
  public function writeJson($url, $class){
    if (mb_strtolower($class) == "git") {
      $fileName = mb_strtolower($class) . '-' . $url . '.json';
    } else {
      $fileName = mb_strtolower($class) . '.json';
    }
    $class = new $class;
    $json = $class->getContent($url);
    $file = fopen('json/' . $fileName, 'w');
    fwrite($file, $json);
    fclose($file);
    echo 'File <em>' . $fileName . '</em> written in json directory. <br>';
  }

  public function getGlobalContent(){
    $globalAr = [];
    $fileName = 'global.json';
    $typJson = file_get_contents('json/typotheque.json');
    $resJson = file_get_contents('json/ressources.json');
    $blogJson = file_get_contents('json/blog.json');
    $gitJson = file_get_contents('json/gitGlobal.json');
    $gitJson = file_get_contents('content/json/screenshots.json');
    // $gitJson = file_get_contents('json/screenshots.json');
    $globalAr['ressources'] = json_decode($resJson);
    $globalAr['blog'] = json_decode($blogJson);
    $globalAr['typotheque'] = json_decode($typJson);
    $globalAr['screenshots'] = json_decode($typJson);
    // $globalAr['screenshots'] = json_decode($screenJson);
    foreach (json_decode($gitJson) as $keys => $value) {
      $globalAr[$keys] = $value;
    }

    $globalGood = [];
    foreach ($globalAr as $keys => $value) {
      foreach ($value as $key => $value) {
        $globalGood[$key] = $value;
      }
    }
    krsort($globalGood);
    $globalGood = json_encode($globalGood);
    $file = fopen('json/' . $fileName, 'w');
    fwrite($file, $globalGood);
    fclose($file);
    echo 'File <em>' . $fileName . '</em> written in json directory. <br>';
  }


    public function MergeGits(){
      $globalAr = [];
      $fileName = 'gitGlobal.json';

      $gitJson = file_get_contents('json/git-1917441.json');
      $gitToolsJson = file_get_contents('json/git-2126033.json');
      $gitFoundryJson = file_get_contents('json/git-2146438.json');
      $gitLuuseIoJson = file_get_contents('json/git-2052442.json');
      $gitVillaJson = file_get_contents('json/git-2052241.json');

      $globalAr['git-1917441'] = json_decode($gitJson);
      $globalAr['git-2126033'] = json_decode($gitToolsJson);
      $globalAr['git-2146438'] = json_decode($gitFoundryJson);
      $globalAr['git-2052442'] = json_decode($gitLuuseIoJson);
      $globalAr['git-2052241'] = json_decode($gitVillaJson);

      $globalGood = [];
      foreach ($globalAr as $keys => $value) {
        foreach ($value as $key => $value) {
          $globalGood[$keys][$key] = $value;
        }
      }

      $globalGood = json_encode($globalGood);
      $file = fopen('json/' . $fileName, 'w');
      fwrite($file, $globalGood);
      fclose($file);
      echo 'File <em>' . $fileName . '</em> written in json directory. <br>';
    }

}
