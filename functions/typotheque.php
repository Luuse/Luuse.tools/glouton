<?php

  require "vendor/autoload.php";
  use PHPHtmlParser\Dom;

  class Typotheque extends GetContent  {
    private function treatjson($url){
      $this->geturl = file_get_contents($url);
      $this->json = json_decode($this->geturl);
      return $this->json;
    }
    public function getContent($url){
      $items = $this->treatjson($url);
      $fonts = array();
      $i = 0;
      print_r($items);
      $this->url = 'http://typotheque.luuse.io/content/fonts/';
      foreach ($items as $key => $font) {
        $uniqId = uniqid();
        $date = date('Ymdhm', $font->created);
        $fonts[$date . '-' . $uniqId ]['type'] = 'typotheque';
        $fonts[$date . '-' . $uniqId ]['url'] = $this->url;
        $fonts[$date . '-' . $uniqId ]['folderName'] = $font->folderName;
        $fonts[$date . '-' . $uniqId ]['family'] = $font->family;
        $fonts[$date . '-' . $uniqId ]['designer'] = $font->designer;
        $fonts[$date . '-' . $uniqId ]['types'] = $font->types;
        $fonts[$date . '-' . $uniqId ]['fileNames'] = $font->fonts->filenames;
        $fonts[$date . '-' . $uniqId ]['styles'] = $font->fonts->styles;
      }

      return json_encode($fonts);

    }
  }
