<?php

  require "vendor/autoload.php";
  use PHPHtmlParser\Dom;

  class Blog extends GetContent  {

    function getContent($url){
      $html = file_get_contents($url);
      $dom = new Dom;
      $dom->load($html);
      $articleEl = $dom->find('li');
      $articles = array();
      foreach ($articleEl as $el) {
        $url = $el->find('a')->getAttribute('data-url');
        $title = $el->find('a')->text();
        $date = count($el->find('h2')) > 0 ? $el->find('h2')->text() : 'null';
        $img = count($el->find('img')) > 0 ? $el->find('img')->getAttribute('src') : 'null';
        $articles[$title]['type'] = 'blog';
        $articles[$title]['date'] = $date;
        $articles[$title]['url'] = $url;
        $articles[$title]['img'] = $img;
      }
      return json_encode($articles);
    }

  }
