<?php
// id luuse  1917441
class Git extends GetContent {
  private $url_gitlab = 'https://gitlab.com/api/v4/';

  private function treatjson($id){
    $this->geturl = file_get_contents($this->url_gitlab . 'groups/' . $id  );
    $this->json = json_decode($this->geturl);
    return $this->json->projects;
  }

  public function getContent($id){
    $items = $this->treatjson($id);
    $c = 0;
    $gitCommits = array();
    foreach ($items as &$item) {
      $this->projects[$c]->id = $item->id;
      $this->projects[$c]->name = $item->name;
      $this->projects[$c]->date = date_parse($item->created_at);
      $this->projects[$c]->url = $item->http_url_to_repo;
      $this->projects[$c]->commits = file_get_contents($this->url_gitlab . 'projects/' . $this->projects[$c]->id . '/repository/commits');
      $commits = (json_decode($this->projects[$c]->commits));
      $git[$this->projects[$c]->id]['name'] = $this->projects[$c]->name;
      $git[$this->projects[$c]->id]['date'] = $this->projects[$c]->date;
      foreach ($commits as $i => $commit) {
        $uniqId = uniqid();
        $date = date('Ymdhm', strtotime($commit->authored_date));
        $gitCommits[$date . '-' . $uniqId]['type'] = 'git';
        $gitCommits[$date . '-' . $uniqId]['title'] = $commit->title;
        $gitCommits[$date . '-' . $uniqId]['project'] = $this->projects[$c]->name;
        $gitCommits[$date . '-' . $uniqId]['url'] = $this->projects[$c]->url;
        $gitCommits[$date . '-' . $uniqId]['author'] = $commit->author_name;
        $gitCommits[$date . '-' . $uniqId]['date'] = $date;
        $gitCommits[$date . '-' . $uniqId]['id'] = $commit->id;
      }
      $c++;
    }
    return json_encode($gitCommits);
  }


}
