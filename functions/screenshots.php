<?php

  class Screenshots extends GetContent  {
    private function treatjson($url){
      $this->geturl = file_get_contents($url);
      $this->json = json_decode($this->geturl);
      return $this->json;
    }
      public function getContent($url){
        $items = $this->treatjson($url);
        $screenshots = array();
        $i = 0;
        $this->url = 'http://typotheque.luuse.io/content/fonts/';
        foreach ($items as $key => $screenshot){
          $uniqId = uniqid();
          $date = date('Ymdhm',  strtotime($key));
          $screenshots[''][$date . '-' . $uniqId]['type'] = 'screenshots';
          $screenshots[''][$date . '-' . $uniqId]['path'] = $screenshot;
        }

        print_r($screenshots);
        return json_encode($screenshots);

      }

  }
